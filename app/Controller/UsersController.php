<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public $helpers = array('Html', 'Form');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('add','login');
    }

    public function index()
    {
        if ($this->Auth->login()) {
            $this->set(array('users'=>$this->User->find('all'),'current_user'=>$this->Auth->user()));
        }else{
            $this->Flash->success('Авторизуйтесь или зарегистрируйтесь');
            return $this->redirect('login');
        }
    }

    public function login()
    {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect('/');
            }
            $this->Flash->error('Неправильно введен логин/пароль');
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function view($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException('Пользователь не найден');
        }
        $this->set('user', $this->User->findById($id));
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Flash->success('Пользователь сохранен');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error('Пользователь не сохранен, попробуйте снова');
        }
    }

    public function edit($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException('Пользователь не найден');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success('Пользователь сохранен');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error('Пользователь не сохранен, попробуйте снова');
        } else {
            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }

    public function delete($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException('Пользователь не найден');
        }
        if ($this->User->delete()) {
            $this->Flash->success('Пользователь удален');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error('Пользователь не удален, попробуйте снова');
        return $this->redirect(array('action' => 'index'));
    }

}