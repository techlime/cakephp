<h1>Пользователи</h1>
<?php echo $this->Html->link('Выйти',
    array('controller' => 'users', 'action' => 'logout'), ['style' => 'float: right']); ?>
<?php echo $this->Html->link('Добавить пользователя',
    array('controller' => 'users', 'action' => 'add'), ['style' => 'float: right;margin-right:10px']); ?>
<table>
    <tr>
        <th>Id</th>
        <th>Username</th>
        <th>Создан</th>
        <th>Операции</th>
    </tr>

    <?php foreach ($users as $user): ?>
        <tr>
            <td><?php echo $user['User']['id']; ?></td>
            <td>
                <?php echo $user['User']['username']; ?>
            </td>
            <td><?php echo $user['User']['created']; ?></td>
            <td>
                <?php echo $this->Html->link('Посмотреть',
                    array('controller' => 'users', 'action' => 'view', $user['User']['id'])); ?>
                <?php echo $this->Html->link('Редактировать',
                    array('controller' => 'users', 'action' => 'edit', $user['User']['id'])); ?>
                <?php if($current_user['id']!=$user['User']['id']){
                        echo $this->Html->link('Удалить',
                            array('controller' => 'users', 'action' => 'delete', $user['User']['id']));
                        } ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <?php unset($user); ?>
</table>