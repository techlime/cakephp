<table>
    <tr>
        <th>Id</th>
        <th>Username</th>
        <th>Создан</th>
        <th>Операции</th>
    </tr>
        <tr>
            <td><?php echo $user['User']['id']; ?></td>
            <td>
                <?php echo $user['User']['username']; ?>
            </td>
            <td><?php echo $user['User']['created']; ?></td>
            <td>
                <?php echo $this->Html->link('Редактировать',
                    array('controller' => 'users', 'action' => 'edit', $user['User']['id'])); ?>
                <?php echo $this->Html->link('Удалить',
                    array('controller' => 'users', 'action' => 'delete', $user['User']['id'])); ?>
            </td>
        </tr>
    <?php unset($user); ?>
</table>